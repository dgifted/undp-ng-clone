window.ProjectRey = window.ProjectRey || {};
ProjectRey.onDomReady = ProjectRey.onDomReady || [];
ProjectRey.onDomReady.push(function(){
	
	$('[data-exploremore]').each(function(i, ele) {
		var $this = $(this);
		var endpoint = $this.data('endpoint').replace('jcr:content', '_jcr_content') + '.html';
		if (!$this.data('cacheable')) {
			endpoint += '?_=' + Date.now();
		}
		$.get(endpoint, function(content) {
			$this.html(content);
			
			// add dummy cards to pad list nicely for desktops
			/*if (Foundation.MediaQuery.atLeast('large')) {
				var $container = $this.find('.cards-container');
				var remainder = $container.children('.card').length % 3;
				if (remainder > 0) {
					for (var i = 0; remainder < 3; remainder++) {
						$('<div class="card invisible"></div>').appendTo($container);
					}
				}
			}*/
			if (typeof $this.data('equalizer') != 'undefined') {
				new Foundation.Equalizer($this, $this.data());
			}
			if (typeof Waypoint != 'undefined') {
				Waypoint.refreshAll();
			}
		});
	});
	
	
	$(document).on('click', '[data-exploremore] .exploremore-loadnext', function(e) {
		e.preventDefault();
		var $button = $(this);
		var $parent = $button.parents('[data-exploremore]');
		var $container = $parent.find('.cards-container');
		var endpoint = $button.attr('href');
		
		$.get(endpoint, function(content) {
			var $content = $(content);
			var $cards = $content.find('[data-card]');
			$container.append($cards);
			// update new target for loading more content
			var newendpoint = $content.find('.exploremore-loadnext').attr('href');
			if (typeof newendpoint != 'undefined' && newendpoint != endpoint) {
				$button.attr('href', $content.find('.exploremore-loadnext').attr('href'));
			} else {
				$button.remove();
			}

			if (typeof $parent.data('equalizer') != 'undefined') {
				new Foundation.Equalizer($parent, $parent.data());
			}
			
			if (typeof Waypoint != 'undefined') {
				Waypoint.refreshAll();
			}			
			
		});		
		return false;
	});
	
});
