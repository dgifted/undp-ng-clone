//download.js v4.21, by dandavis; 2008-2018. [MIT] see http://danml.com/download.html for tests/usage
// v1 landed a FF+Chrome compatible way of downloading strings to local un-named files, upgraded to use a hidden frame and optional mime
// v2 added named files via a[download], msSaveBlob, IE (10+) support, and window.URL support for larger+faster saves than dataURLs
// v3 added dataURL and Blob Input, bind-toggle arity, and legacy dataURL fallback was improved with force-download mime and base64 support. 3.1 improved safari handling.
// v4 adds AMD/UMD, commonJS, and plain browser support
// v4.1 adds url download capability via solo URL argument (same domain/CORS only)
// v4.2 adds semantic variable names, long (over 2MB) dataURL support, and hidden by default temp anchors
// https://github.com/rndme/download

(function (root, factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module.
		define([], factory);
	} else if (typeof exports === 'object') {
		// Node. Does not work with strict CommonJS, but
		// only CommonJS-like environments that support module.exports,
		// like Node.
		module.exports = factory();
	} else {
		// Browser globals (root is window)
		root.download = factory();
  }
}(this, function () {

	return function download(data, strFileName, strMimeType) {

		var self = window, // this script is only for browsers anyway...
			defaultMime = "application/octet-stream", // this default mime also triggers iframe downloads
			mimeType = strMimeType || defaultMime,
			payload = data,
			url = !strFileName && !strMimeType && payload,
			anchor = document.createElement("a"),
			toString = function(a){return String(a);},
			myBlob = (self.Blob || self.MozBlob || self.WebKitBlob || toString),
			fileName = strFileName || "download",
			blob,
			reader;
			myBlob= myBlob.call ? myBlob.bind(self) : Blob ;
	  
		if(String(this)==="true"){ //reverse arguments, allowing download.bind(true, "text/xml", "export.xml") to act as a callback
			payload=[payload, mimeType];
			mimeType=payload[0];
			payload=payload[1];
		}


		if(url && url.length< 2048){ // if no filename and no mime, assume a url was passed as the only argument
			fileName = url.split("../../../../../../index.html").pop().split("?")[0];
			anchor.href = url; // assign href prop to temp anchor
		  	// if(anchor.href.indexOf(url) !== -1){ // if the browser determines that it's a potentially valid url path:
            if(decodeURI(anchor.href).indexOf(decodeURI(url)) !== -1){
        		var ajax=new XMLHttpRequest();
        		ajax.open( "GET.html", url, true);
        		ajax.responseType = 'blob';
        		ajax.onload= function(e){ 
				  download(e.target.response, fileName, defaultMime);
				};
        		setTimeout(function(){ ajax.send();}, 0); // allows setting custom ajax headers using the return:
			    return ajax;
			} // end if valid url?
		} // end if url?


		//go ahead and download dataURLs right away
		if(/^data:([\w+-]+\/[\w+.-]+)?[,;]/.test(payload)){
		
			if(payload.length > (1024*1024*1.999) && myBlob !== toString ){
				payload=dataUrlToBlob(payload);
				mimeType=payload.type || defaultMime;
			}else{			
				return navigator.msSaveBlob ?  // IE10 can't do a[download], only Blobs:
					navigator.msSaveBlob(dataUrlToBlob(payload), fileName) :
					saver(payload) ; // everyone else can save dataURLs un-processed
			}
			
		}else{//not data url, is it a string with special needs?
			if(/([\x80-\xff])/.test(payload)){			  
				var i=0, tempUiArr= new Uint8Array(payload.length), mx=tempUiArr.length;
				for(i;i<mx;++i) tempUiArr[i]= payload.charCodeAt(i);
			 	payload=new myBlob([tempUiArr], {type: mimeType});
			}		  
		}
		blob = payload instanceof myBlob ?
			payload :
			new myBlob([payload], {type: mimeType}) ;


		function dataUrlToBlob(strUrl) {
			var parts= strUrl.split(/[:;,]/),
			type= parts[1],
			indexDecoder = strUrl.indexOf("charset")>0 ? 3: 2,
			decoder= parts[indexDecoder] == "base64" ? atob : decodeURIComponent,
			binData= decoder( parts.pop() ),
			mx= binData.length,
			i= 0,
			uiArr= new Uint8Array(mx);

			for(i;i<mx;++i) uiArr[i]= binData.charCodeAt(i);

			return new myBlob([uiArr], {type: type});
		 }

		function saver(url, winMode){

			if ('download' in anchor) { //html5 A[download]
				anchor.href = url;
				anchor.setAttribute("download", fileName);
				anchor.className = "download-js-link";
				anchor.innerHTML = "downloading...";
				anchor.style.display = "none";
 				anchor.addEventListener('click', function(e) {
 					e.stopPropagation();
 					this.removeEventListener('click', arguments.callee);
 				});
				document.body.appendChild(anchor);
				setTimeout(function() {
					anchor.click();
					document.body.removeChild(anchor);
					if(winMode===true){setTimeout(function(){ self.URL.revokeObjectURL(anchor.href);}, 250 );}
				}, 66);
				return true;
			}

			// handle non-a[download] safari as best we can:
			if(/(Version)\/(\d+)\.(\d+)(?:\.(\d+))?.*Safari\//.test(navigator.userAgent)) {
				if(/^data:/.test(url))	url="data:"+url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
				if(!window.open(url)){ // popup blocked, offer direct download:
					if(confirm("Displaying New Document\n\nUse Save As... to download, then click back to return to this page.")){ location.href=url; }
				}
				return true;
			}

			//do iframe dataURL download (old ch+FF):
			var f = document.createElement("iframe");
			document.body.appendChild(f);

			if(!winMode && /^data:/.test(url)){ // force a mime that will download:
				url="data:"+url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
			}
			f.src=url;
			setTimeout(function(){ document.body.removeChild(f); }, 333);

		}//end saver




		if (navigator.msSaveBlob) { // IE10+ : (has Blob, but not a[download] or URL)
			return navigator.msSaveBlob(blob, fileName);
		}

		if(self.URL){ // simple fast and modern way using Blob and URL:
			saver(self.URL.createObjectURL(blob), true);
		}else{
			// handle non-Blob()+non-URL browsers:
			if(typeof blob === "string" || blob.constructor===toString ){
				try{
					return saver( "data:" +  mimeType   + ";base64,"  +  self.btoa(blob)  );
				}catch(y){
					return saver( "data:" +  mimeType   + "," + encodeURIComponent(blob)  );
				}
			}

			// Blob but not URL support:
			reader=new FileReader();
			reader.onload=function(e){
				saver(this.result);
			};
			reader.readAsDataURL(blob);
		}
		return true;
	}; /* end download() */
}));
var dsurvey = (function () {
	
	var probe = 'download-survey-solicited'; // name of the storage variable
	var popuptimeout = 1000; // 1 sec - delay after file download click and survey pop up (milliseconds)
	var cooldown = 30*24*60*60*1000; // 1 month - delay before we display popup to users who already denied to participate (milliseconds)
	var endpoint = 'https://script.google.com/macros/s/AKfycbx_s0dxRQ7_KxBfJnxf_tKR0zzUfrtFFaqwNRbFQYxxMM2w0oEn/exec'; // Google App script writing results to the Sheet
	var reValidEmail = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i; // regular expression to test strings if contain valid email
	var form = '#publication-exitsurvey';	
	
	function trackFileDownload(url, title) {
		
		try {
			
			var info = $.parseJSON(localStorage.getItem(probe));			
			
			if (info.email != '') {
				// user already gave us permission, just record another download
				recordDownload({
					email: info.email, 
					url: url, 
					title: title
				});
			} else if ( (new Date()).getTime() - info.timestamp > cooldown) {
				// cooldown period already passed, let's ask again
				throw "Ask again";
			}
			
		} catch ( err ) {
			// first time or 'long-time-no-see' user
			showSurvey(url, title);
		}

	}
	
	function recordDownload(data) {		
		var input = {
			email: data.email,
			publication_url: location.href,
			publication_title: $('title').text().split(' | ')[0],
			file_url: data.url,
			file_title: data.title,
			topic: data.topics || '',
			country: data.countries || '',
			types: data.types || '',
			pubDate: data.pubDate || ''
		};
		
		$.post(endpoint, input);
	}
	
	function showSurvey(url, title) {

		$(form).data('url', url).data('title', title);
		
		// Open model when using Foundation
		if ($(form).foundation) {
			$(form).foundation('open');
		} else {
			// Open model when using bootstrap
			$(form).modal('show');
		}
		
		localStorage.setItem(probe, getProbe());
		
	}
	
	function getProbe (email) {
		var result = {
			email: (typeof email !== 'undefined') ? email : '',
			timestamp: (new Date()).getTime()
		}; 
		return JSON.stringify(result);
	}
	
	return {
		
		wireSurvey: function($selector, event) {
			
			var filetypes = /\.(zip|pdf|doc.*|xls.*|ppt.*|mp3|txt|rar|csv)$/i;
			var href = $selector.attr('href');
			
			if (href && href.startsWith('/content/dam') || href.match(filetypes) != null) {
			
				$selector.on(event, function(e) {
					e.preventDefault();
					
					var $this = $(this);
					var href = $this.attr('href');
					
					window.setTimeout( function() { trackFileDownload(href, $this.text());} , popuptimeout);
					
					download(href);
					
				});
			}
				
		},
		
		processSubmission: function() {

            var data = {
        		email: $('input[type=email]', form).val().trim().toLowerCase(),
                url: $(form).data('url'),
                title: $(form).data('title'),
                topics: $('input[name="topics"]', form).val().trim(),
                countries: $('input[name="countries"]', form).val().trim(),
                types: $('input[name="types"]', form).val().trim(),
                pubDate: $('input[name="pubDate"]', form).val().trim()	
            };
            
            if (reValidEmail.test(data.email)) {
				
				// Close model when using Foundation
				if ($(form).foundation) {
					$(form).foundation('close');
				} else {
					// Close model when using bootstrap
					$(form).modal('hide');
				}
				
				recordDownload(data);
				
				localStorage.setItem(probe, getProbe());
				
				$('form', form).removeClass('has-error');
				
			} else {
				
				$('form', form).addClass('has-error');
				
			}			
		},
		
		form : form
	
	}
})();

// we need to make sure we have an ability to save the info 
window.ProjectRey = window.ProjectRey || {};
ProjectRey.onDomReady = ProjectRey.onDomReady || [];
ProjectRey.onDomReady.push(function(){
	if (typeof localStorage === "object") {
		$(function(){
			
			$('.publication .card-section a').each(function() {
				var $this = $(this);
				dsurvey.wireSurvey($this, 'click');
			});
			
			$('form', dsurvey.form).on('submit', function(event){			
				event.preventDefault();
				dsurvey.processSubmission();			
				return false;
			});
			
		});	
	}
});
