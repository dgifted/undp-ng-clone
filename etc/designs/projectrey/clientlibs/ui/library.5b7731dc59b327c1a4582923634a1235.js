window.ProjectRey = window.ProjectRey || {};
ProjectRey.onDomReady = ProjectRey.onDomReady || [];
ProjectRey.onDomReady.push(function(){
	
	var $results = $('[data-library-results]');
	
	var renderResults = function(start, callback) {
		
		var options = {
			tagid: collectTags(), 
			q: $results.data('keyword'),
			start: start || 1,
			sort: $('#results-order option:selected').val()
		};
		
		$.get($results.data('endpoint') + '.html', options, function(response) {
			
			if (typeof callback == 'function') {
				callback.call(this, response);
			} else {
				var $response = $(response);
				var totalMatches = $response.data('totalMatches') || 0;
				updateSearchInfo(totalMatches, options.start);
				
				$results.empty().append($response);
				
			}
		});
	};
	
	var updateSearchInfo = function(totalMatches, start) {
		var $rs = $('#results-stats').toggleClass('invisible', totalMatches < 2).find('span');
		var limit = parseInt($results.data('limit'));
		var from = Math.max(limit * (start-1) + 1, 0);
		var to = Math.min(totalMatches, from + limit - 1);
		$rs.eq(0).text(from + '-' + to);
		$rs.eq(1).text(totalMatches);			
	}
	
	var collectTags = function() {
		var tagid = [];
		
	    $('#library-filters input[type="checkbox"]:checked').each(function(){
	   	 	tagid.push($(this).val());
	    });
	    
	    var tags = tagid.join(',');
	    $results.data('tagid', tags);
	    
	    return tags;
	}
	
	// prevent link clicks for checkbox labels
	$('#library-filters input[type="checkbox"]').on('click', function (e) {
	     e.stopPropagation();
	     renderResults();
    });
	
	// pre-select tags defined via properties
	if ($results.data('tagid')) {
		var tagids = $results.data('tagid').split(',');
		for (var i = 0; i < tagids.length; i++) {
			$('#library-filters input[value="' + tagids[i] + '"]').attr({'checked': true, 'disabled': true});
		}
	}
	
	// init Order By select box
	$('#results-order').on('change', function(){
		renderResults();
	});
	
	// pagination
	$(document).on('click', '.library-pagination a', function(e) {
		e.preventDefault();
		var start = $(this).data('start');
		renderResults(start);		
	}).on('click','.library-pagination button',  function(){
		// load more button
		var $this = $(this);
		var currentSize = $results.find('.cell').length; // already loaded publications
		var limit = parseInt($results.data('limit'));
		var start = Math.ceil(currentSize / limit); // current page
		
		renderResults(start+1, function(response){
			
			var $response = $(response);
			var totalMatches = $response.data('totalMatches') || 0;
			var size = $results.find('.library-results').append($response.find('.cell')).find('.cell').length;
			
			$('#results-stats').find('span').eq(0).text('1-' + size);
			
			if (size >= parseInt(totalMatches)) {
				$this.remove();
			}
			
		});		
		
	});
	
	// reset button
	$('#library-filters [data-filters-reset]').on('click', function(){
		$('#library-filters input[type="checkbox"]:checked').not(':disabled').prop('checked', false);
		renderResults();
	});
	
	renderResults();
	
});

