(function(window, document, $) {
	$(function(){	  
	  // sort dropdown options alphabetically
	  $('.list-filter select').each(function(e,i){
	    var $this = $(this);
	    var val = $this.val();
	    var $options = $this.find('option');
	    $options.sort(function(a,b){
	      return a.value.localeCompare(b.value);
	    });
	    $this.html($options).val(val);
	  }).on('change', function(e){
		// what to do on filter change
		  $('.list-filter form').submit();
		/*   var params = '';
	   $('.list-filter-dropdown').each(function(){
	     var $this = $(this);
	     if ($this.val() != '') {
	       if (params != '') {
	         params += '&';
	       } 
	       params += $this.prop('name') + '=' + escape($this.val());
	     }
	   });
	   window.location.href = (params == '') ? window.location.pathname : window.location.pathname + '?' + params;*/
	  });
	});
})(window, document, jQuery);
