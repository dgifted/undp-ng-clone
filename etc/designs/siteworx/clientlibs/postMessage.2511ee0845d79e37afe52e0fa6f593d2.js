var SWX = SWX || {}
var PostMessage = PostMessage || {

    initialized: false,

    messageHandler: function(e) {
        //Validate the sender
        if(e.origin === "http://www.open.undp.org/") {
            var data = $.parseJSON(e.data);
            //Retrieve the height of the iframe
            $("iframe[src='" + data.source + "'").height(data.height + "px");
        }
    },

    init: function(force) {
        if(!this.initialized || force) {
            if(window.addEventListener) {
                window.addEventListener("message", this.messageHandler, false);
            }else {
                window.attachEvent("onmessage", this.messageHandler);
            }
        }
    }
}
